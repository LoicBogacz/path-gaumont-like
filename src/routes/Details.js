import React, { Component } from "react";
import axios from "axios";

import { Spinner, Banner, ActorList } from "../components";
import { API_URL, API_KEY } from "../config";

class Details extends Component {
    state = {
        loading: false,
        actors: [
            {
                name: "Marc Lala"
            },
            {
                name: "Marc Lala"
            },
            {
                name: "Marc Lala"
            },
            {
                name: "Marc Lala"
            }
        ],
        mTitle: "Star Wars",
        mDescription: "Lorem Ipsum Lorem Isupm",
        imgSrc: '../../public/images/Fast_large.jpg',
        runtime: "2h30",
        revenue: "$2121212",
        status: "Released",
        vote: ""
    };

    async componentDidMount() {
        try {
            const movieId = this.props.match.params.id;
            const url = `${API_URL}/movie/${movieId}?api_key=${API_KEY}&language=en`;

            const {
                data : {
                    title,
                    overview,
                    runtime,
                    revenue,
                    status,
                    vote_average,
                    poster_path
                }
            } = await this.loadInfos(url);

            this.setState({
                mTitle: title,
                mDescription: overview,
                imgSrc: poster_path,
                revenue,
                runtime,
                status,
                vote: vote_average
            }, async () => {
                const creditsUrl = `${API_URL}/movie/${movieId}/credits?api_key=${API_KEY}&language=en`;

                const { data: { cast }} = await this.loadInfos(creditsUrl);
                this.setState({ actors: [...cast], loading: false });
            })
        } catch(e) {
            console.log('error: ', e);
        }
    }

    loadInfos = url => axios.get(url);

    render() {
        const { loading, actors, mTitle, mDescription, imgSrc, runtime, revenue, status, vote } = this.state;

        return (
            <div className="App">
                {loading ?
                    (
                        <Spinner />
                    ):
                    (
                        <>
                            <Banner
                                mTitle={mTitle}
                                mDescription={mDescription}
                                imgSrc={imgSrc}
                                runtime={runtime}
                                revenue={revenue}
                                status={status}
                                vote={vote}
                            />

                            <ActorList actors={actors} />
                        </>
                    )
                }
            </div>
        );
    }
}

export { Details };