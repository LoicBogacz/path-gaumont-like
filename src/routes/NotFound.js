import React from "react";

const NotFound = props => (
        <div>
            <p>404 : PAGE NOT FOUND</p>
        </div>
);

export { NotFound };