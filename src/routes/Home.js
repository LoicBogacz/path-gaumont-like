import React, { Component } from "react";

import { HeaderImg, SearchBar, PosterList, LoadButton } from "../components";

class Home extends Component {
    render() {
        const { mTitle, mDescription, image, movies, loading } = this.props;

        return (
            <div>
                <HeaderImg title={mTitle}
                           description={mDescription}
                           imgSrc={image}
                />
                <SearchBar onSearchClick={this.props.onSearchClick} />
                <PosterList movies={movies} />
                <LoadButton
                    loading={loading}
                    onButtonClick={this.props.onButtonClick}
                />
            </div>
        )
    }
}

export { Home };