import React from "react";
import FontAwesome from "react-fontawesome";

import "../../css/Stars.css";

const Stars = props => {
    const renderFilledStars = () => {
        return props.fakeFilledArray.map((element, i) => {
            return (
                <FontAwesome
                    key={i}
                    className="stars"
                    name="star"
                    size="3x"
                />
            )
        })
    };

    const renderEmptyStars = () => {
        return props.fakeEmptyArray.map((element, i) => {
            return (
                <FontAwesome
                    key={i}
                    className="stars"
                    name="star-o"
                    size="3x"
                />
            )
        })
    };

    return (
        <div className="stars">
            {renderFilledStars()}
            {renderEmptyStars()}
        </div>
    )
};

export { Stars };