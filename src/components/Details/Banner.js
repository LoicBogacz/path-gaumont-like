import React, { Component } from "react";

import { Container, Stars } from "../index";
import { IMAGE_BASE_URL, POSTER_SIZE } from "../../config";
import { calcTime, convertMoney } from "../../utils/helpers";

import "../../css/Banner.css";

class Banner extends Component {
    calcVotes = () => {
        this.fakeFilledArray = [];
        this.fakeEmptyArray = [];
        const filledStars = Math.round(this.props.vote / 2);
        const emptyStars = 5 - filledStars;

        for(let i = 0; i < filledStars; i++) {
            this.fakeFilledArray.push("1");
        }

        if(emptyStars !== 0) {
            for(let i = 0; i < emptyStars; i++) {
                this.fakeEmptyArray.push("1");
            }
        }
    };

    render() {
        this.calcVotes();
        const imgSrc = `${IMAGE_BASE_URL}/${POSTER_SIZE}/${this.props.imgSrc}`;

        return (
            <div className="headerDetails">
                <div className="badge-decoration">{this.props.status}</div>
                <div className="headerDetails--poster">
                    <img className="headerDetails--poster__img" src={imgSrc} alt={this.props.mTitle} />
                </div>
                <div className="headerDetails--container">
                    <h3 className="headerDetails--container__title">{this.props.mTitle}</h3>
                    <p className="headerDetails--container__desc">{this.props.mDescription}</p>
                    <div className="headerDetails--info">
                        <Container iconName="clock" content={calcTime(this.props.runtime)} />
                        <Stars fakeFilledArray={this.fakeFilledArray} fakeEmptyArray={this.fakeEmptyArray} />
                        <Container iconName="money" content={convertMoney(this.props.revenue)} />
                    </div>
                </div>
            </div>
        );
    }
}

export { Banner };