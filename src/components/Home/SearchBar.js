import React, { Component } from "react";
import FontAwesome from "react-fontawesome";

import "../../css/SearchBar.css";

class SearchBar extends Component {
    state = {
        value: ""
    };

    handleChance = e => {
        this.setState({ value: e.target.value });
    };

    render() {
        const { value } = this.state;

        return (
            <div className="searchBar--container">
                <div className="searchBar">
                    <input
                        className="searchBar--input"
                        type="text"
                        placeholder="Search for a movie"
                        value={this.state.value}
                        onChange={this.handleChance}
                    />

                    <div
                        className="searchBar--submit"
                        onClick={() => this.props.onSearchClick(value)}
                    >
                        <FontAwesome className="searchIcon" name="search" />
                    </div>
                </div>
            </div>
        );
    }
}

export { SearchBar };