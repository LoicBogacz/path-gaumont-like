import React, { Component } from "react";

import { IMAGE_BASE_URL, POSTER_SIZE } from "../../config";
import { Poster } from "../index";
import "../../css/PosterList.css";

let wish;

class PosterList extends Component {
    renderPoster = () => {
        return this.props.movies.map(movie => {
            const imgSrc = `${IMAGE_BASE_URL}/${POSTER_SIZE}/${movie.poster_path}`;
            wish = false;

            return (
                <Poster
                    id={movie.id}
                    key={movie.id}
                    imgSrc={imgSrc}
                    hover={false}
                    wished={wish}
                    movie={movie}
                    mTitle={movie.title}
                    mDescription={movie.description}
                />
            )
        })
    };

    render() {
        return (
            <div className="posterList">
                <h3 className="posterList--title">NEW MOVIES</h3>
                <div className="posterList--grid">
                    {this.renderPoster()}
                </div>
            </div>
        )
    }
}
export { PosterList };