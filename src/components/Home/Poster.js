import React, { Component } from "react";
import FontAwesome from "react-fontawesome";
import { Link } from "react-router-dom";

import "../../css/Poster.css";

class Poster extends Component {
    state = {
        hover: false
    };

    showOverlay = () => {
        if(this.state.hover) {
            return;
        }
        this.setState({ hover: true });
    };

    hideOverlay = () => {
        this.setState({ hover: false });
    };

    removeWished = () => {
        console.log('removeWished');
    };

    addWished = () => {
        console.log('addWished');
    };

    render() {
        return (
            <div
                className="poster"
                onMouseEnter={this.showOverlay}
                onMouseLeave={this.hideOverlay}
            >
                <Link to={{ pathname: `/${this.props.id}`}}>
                    <img className="poster--img" src={this.props.imgSrc} alt={this.props.title} />
                </Link>
                {this.state.hover ?
                    (
                        <div className="poster--overlay">
                            <h3 className="poster--overlay__text">WISHLIST</h3>
                            {this.props.wished ?
                                (
                                    <FontAwesome
                                        className="poster--icon"
                                        name="heart"
                                        size="3x"
                                        onClick={this.removeWished}
                                    />
                                ) :
                                (
                                    <FontAwesome
                                        className="poster--icon__not"
                                        name="heart-o"
                                        size="3x"
                                        onClick={this.addWished}
                                    />
                                )}
                        </div>
                    ) : null
                }
            </div>
        )
    }
}

export { Poster };