import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

library.add(faSpinner);

const Spinner = props => {
    return (
        <FontAwesomeIcon
            icon="spinner"
            pulse
            size="7x"
            className="fa fa-faSpinner"
        />
    );
};

export { Spinner };