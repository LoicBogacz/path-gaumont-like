import React, { Component } from 'react';
import axios from "axios";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import { API_URL, API_KEY, IMAGE_BASE_URL, BACKDROP_SIZE } from "./config";
import { Home, Details, NotFound } from "./routes";
import { Header, Spinner } from "./components";
import './App.css';

class App extends Component {
    state = {
        loading: true,
        movies: [],
        badge: 0,
        image: null,
        mTitle: "",
        mDescription: "",
        activePage: 0,
        totalPages: 0,
        searchText: ""
    };

    async componentDidMount() {
        try {
            const { data : { results, page, total_pages }} = await this.loadMovies();
            this.setState({
                movies: results,
                loading: false,
                activePage: page,
                totalPages: total_pages,
                image: `${IMAGE_BASE_URL}/${BACKDROP_SIZE}/${results[1].backdrop_path}`,
                mTitle: results[1].title,
                mDescription: results[1].overview,
            })
        } catch (e) {
            console.log('error: ', e);
        }
    }

    loadMovies = () => {
        const activePage = this.state.activePage + 1;
        const url = `${API_URL}/movie/popular?api_key=${API_KEY}&page=${activePage}&language=en`;

        return axios.get(url);
    };

    loadMore = async () => {
        try {
            this.setState({ loading: true });
            const { data : { results, page, total_pages }} = await this.loadMovies();
            this.setState({
                movies: [...this.state.movies, ...results],
                loading: false,
                activePage: page,
                totalPages: total_pages
            })
        } catch(e) {
            console.log(e);
        }
    };

    handleSearch = value => {
        try {
            this.setState({ loading: true, searchText: value, image: null }, async () => {
                const { data : { results, page, total_pages }} = await this.searchMovie();
                this.setState({
                    movies: results,
                    loading: false,
                    activePage: page,
                    totalPages: total_pages,
                    image: `${IMAGE_BASE_URL}/${BACKDROP_SIZE}/${results[0].backdrop_path}`,
                    mTitle: results[0].title,
                    mDescription: results[0].overview,
                })
            });
        } catch (e) {
            console.log('error: ', e);
        }
    };

    searchMovie = () => {
        const url = `${API_URL}/search/movie?api_key=${API_KEY}&query=${this.state.searchText}&language=en`;

        return axios.get(url);
    };

    render() {
        return (
            <BrowserRouter>
                <div className="App">
                    <Header badge={this.state.badge} />
                    {!this.state.image ?
                        (
                            <Spinner />
                        ):
                        (
                            <Switch>
                                <Route path="/" exact render={() => (
                                    <Home
                                        {...this.state}
                                        onSearchClick={this.handleSearch}
                                        onButtonClick={this.loadMore}
                                    />
                                )}
                                />

                                <Route path="/:id" exact component={ Details } />

                                <Route component={ NotFound } />
                            </Switch>
                        )}
                </div>
            </BrowserRouter>
        )
    }
}

export default App;
